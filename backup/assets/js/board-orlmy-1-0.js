(function($)
{
	$.fn.orlmy = function(options)
	{
		var defaults = {socket: null}
		var options =  $.extend(defaults, options);	

		var cls = null;
	    this.each(function(e)
	    { 
	      //initialize the class
	      cls = new j(options.socket);
	    });
	    return cls;
	}

	//set up the class
var j = (function()
    {
      function j(s)
      {
      		var w = new Spliter();
          	var a = new App();
          	a.initAuths(s);
          	a.initXtures();
          	a.initCom(s, w);          	
      }

      
    //return class object
    return j;

  }());


	function Spliter()
	{
		this.worker = null;
		this.StartWorker = function()
		{        
	        if(typeof(Worker) !== "undefined"){
	        	//C:\xampp\htdocs\phynix_app\assets\vendors\orlmy\worker
	            return this.worker = new Worker("../vendors/orlmy/worker/comm.js");
	        }
		}
		this.WorkerSender = function(data)
		{
			this.worker.postMessage(data);
		}
		this.WorkerResponse = function(data)
		{
			if(typeof(Worker) !== null){
				this.worker.onmessage = function(event){
	            	//document.getElementById("result").innerHTML = event.data;
	            	console.log(event.data)
	        	}
	        }
		}
	}

	//controls the layout
	function App()
	{
		//app control
		this.initXtures = function()
		{
			new Xturs().init();
		}
		this.initCom = function(socket, worker){
			var com = new Com();
			com.comEvt(socket, worker);
			//this.uiData(socket, com);
			//initialize the tweet event form App Class
			var tw = new Twit().events(socket);
		}
		this.initAuths = function(socket){
			new Auths().init(socket);
		}
		this.uiData = function(s, com)
		{
			var cookie_string = $.userdata($.key);
			if(typeof(cookie_string)!='undefined'){
				var c = cookie_string.res, user = $(".current-user");
				//set user name
				user.find("h4").html(c.name);
				//store user id in cookie
				$.uucode = c.uucode;
				//set the user id 
				user.attr("id",c.uucode);
			}			
			//this send unique static id associated 
			//to this user to the server
			com.onConnected(s);
			com.onDMMessageReceived(s);
		}

	}

	function Twit(){
		this.events = function(socket){
			var $this = this;
			$(".tweet_post_btn").click(function(){
				var tweet_data_box = $(".tweet_data_box")
				if(tweet_data_box.val() !=""){
					$this.onTweet(socket, tweet_data_box);
					tweet_data_box.val("");
				}				
			});
			this.onReceiveTweet(socket);
		}
		this.onTweet = function(socket, e){
			var data = {"screen_name":"orlmy", "message":e.val()}
			socket.emit('tweet', data);
		}
		this.onReceiveTweet = function(socket){
			var $this = this;
			//initialize the first stream
			//set this based on the app settings
			$this.pullTweet(socket);
			socket.on('tweet_stream_'+$.source, function(data){
				//append tweet to ui
				$this.updateUI(data);
				//simple recursion to request for update
				//from twitter api
				setTimeout(function(){
					$this.pullTweet(socket);
				},15000)
			});	
		}

		this.updateUI = function(data){
			var html = "";
			for(e in data){
				html += '<li>'+
	                    '<div class="badge-wrap">'+
	                        '<div class="more-action">'+
	                            '<div></div><div></div><div></div>'+
	                        '</div>'+
	                        '<div class="timer">2mins</div>'+
	                        '</div>'+
	                        '<div class="profilex" style="background:url('+data[e].user.profile_image_url_https+') no-repeat;background-size:cover;background-position:center"></div>'+
	                        '<h4>'+data[e].user.name+' @'+data[e].user.screen_name+'</h4>'+
	                        '<p>'+data[e].text+'</p>'+
	                        '<p>Retweet '+data[e].retweet_count+' | Like '+data[e].favorite_count+'</p>'+
	                '</li>';
			}

			$(".twitter-tweet-listview").html(html);

		}

		this.pullTweet = function(socket){
			var data = {"screen_name":"orlmy", "tweet_target":'tweet_stream_'+$.source}
			socket.emit('gettweet', data);
			console.log(data)
		}
	}

	function Com(){
		//socket io control
		this.w = null;
		this.comEvt = function(socket, worker){

			var s = socket;
			
			this.w = worker;//set worker instance 
			this.w.StartWorker(); //and initalize workier object

			this.onMessageReceived(s);			
			this.onContactsReceived(s);
			this.onMessegeSend(s);
			//blur and focus
			this.onTypeFocus(s);
			this.onTpypeBlur(s);			
			//status watch
			this.onStatusChanged(s);
			this.onConversationReceived(s);

			this.onCall(s);

			//chat btn control method
			this.onChatInit();
			
		}

		this.onChatInit = function()
		{
			var $this = this;
			var chat = $(".chat-btn"), istyping = $(".istyping")
	        chat.css({cursor:"pointer"})
	        chat.click(function()
	        {

	          	if(!$(this).hasClass("show")){
	          	    $(".chat-box").fadeIn();
	          	    $(this).addClass("show");
	          	    if($(this).hasClass("init"))
	          	    {
	          	    	// var data = {"msg":"Hi! We are here to answer any question you have."};
	          	    	// $this.initProcess(istyping, data);
	          	    	// setTimeout(function()
	          	    	// {
	          	    	// 	//var data = {"msg":"Please, Enter your name."};
	          	    	// 	//$this.initProcess(istyping, data);
	          	    	// }, 1100);	          	    	
	          	    }
	          	    $(this).removeClass("init");
	          	    
	          	}else{
	          	    $(".chat-box").fadeOut();
	          	    $(this).removeClass("show");
	          	}
	        })
		}

		//plugin method control calls outbound
		this.onCall = function(socket)
		{
			$(".dial_btn").click(function()
			{
				var data = {from:'+447449486628', to:'+447340960948'}
				socket.emit('call', data);
			});
		}

		this.onMessageReceived = function(socket)
		{
			$this = this;
			socket.on('dm_'+$.source, function(data){

				//get the sender id
				//check if the user is active
				//else look it up in the contact list
				//apeend the message
				var chat_head = $(".chat-box-head");
				if(chat_head.find("h4").attr("id") == data.source)
				{
					//post to stream
					$this.row_chat_b(data);
				}
				//update list status message 
				$("#"+data.source).find("p").html(data.msg)
				
				//this send back message received status to sender
				//$this.onMessageDelivered(socket);

				//pass message to walker for database login
				$this.w.WorkerSender(data);
			});
		}
		
		this.onConversationReceived = function(socket)
		{
			socket.on("chat_history", function(msg){
				console.log(msg);
			});
		}
		this.onDMMessageReceived = function(socket)
		{
			$this = this;
			//var useruuid = $(".current-user").attr("id");
			
			var uniqueid = $.uniqueid();

			socket.on(uniqueid, function(msg){

					//check if message is for active user
					//else check the contact list
					//then send notification
					var useruuid = $(".active-user-conversation").attr("id");
					var senderid, targetid, message = null;
					senderid = msg.uucode_sender;
					targetid = msg.uucode_target;
					message  = msg.data;
					if(useruuid == senderid){						
						$this.row_chat_b(msg.data);
						$this.onMessageDelivered(socket);
					}
					//search contact list for matching ID
					var r_text = msg.data;
					$(".conversation-list").find("li#"+senderid).find(".status-msg-update").html(r_text);
				
			});
		}

		this.onContactsReceived = function(socket)
		{
			var $this = this;
			socket.on('cc_'+$.source, function(data){
				$this.logNewContact(data);
				//log to database -> create contacts table
			});
		}

		this.logNewContact = function(data){
			
			var list_box = $(".chat-contact-listview");
			var contact_id = data.source;
			var contact = list_box.find("#"+contact_id);
			
			//append contact if not on the list
			if(contact.length == 0)
			{
 			

		        // var html = '<li id="'+contact_id+'" class="contact-chat-list-row">'+
		        //                 '<span>now</span>'+
		        //                 '<div class="list-image">'+
		        //                     '<div style="background:url(orlmy/images/image-1.png) no-repeat; background-position:center;background-size:cover"></div>'+
		        //                 '</div>'+
		        //                 '<div class="list-body row-page">'+
		        //                     '<h4>New User</h4>'+
		        //                     '<p>'+data.msg+'</p>'+
		        //                 '</div>'+
		        //             '</li>';

		        var html = '<li id="'+contact_id+'" class="contact-chat-list-row">'+
                              '<div class="badge-wrap">'+
                                  '<div class="more-action">'+
                                      '<div></div><div></div><div></div>'+
                                  '</div>'+
                                  '<div class="timer">2mins</div>'+
                              '</div>'+
                              '<div class="row-page">'+
		                              '<div class="profilex" style="background:url(assets/images/profiles/image-2.jpg) no-repeat;background-size:cover;background-position:center"></div>'+
		                              '<h4>Matthew Robertson</h4>'+
		                              '<p>'+data.msg+'</p>'+
		                       '</div>'+
                          '</li>';

		            list_box.append(html);
			}

			//notify the board a new user has joined          

            
		}

		this.onMessegeSend = function(socket){
			$this = this;
			$("body").keypress(function(e)
			{
				if(e.which == 13)
				{
					$this.triggerRequest(socket);
					return false;
				}
				
			});

			$(".chat-box-btn-send").click(function()
			{
				$this.triggerRequest(socket);
			})
		}

		this.triggerRequest = function(socket)
		{
			var msg = $('#orlmy-msg').val();
				if(msg != ""){
					var data = {"appid":$.appid,"target":$.target,"source":$.source,"msg":msg,"id":1}
					console.log(JSON.stringify(data))
					socket.emit('dm_message', data);
					$this.row_chat_a(data); 
				}
			$('#orlmy-msg').val('')

		}
		
		this.onStatusChanged = function(socket){

			//get the sender id
			//check if the user is active
				//else look it up in the contact list
				//apeend the message

			var typing = $(".istyping");
			socket.on($.source, function(msg){
				switch(msg.id){
					case 0: typing.hide(); break;//end typing
					case 1: typing.show(); break;//start typing
					case 2:  break;//msg delivered
					case 3:  break;//msg seen/received
				}
			});
		}

		this.onTypeFocus = function(socket){
			//on input field active
			$("#orlmy-msg").on("focus", function(){
				var data = {"appid":$.appid,"target":$.target,"source":$.source,"msg":"","id":1}
				socket.emit('status', data);
			});
		}
		this.onTpypeBlur =  function(socket){
			//code for on input field blur
			$("#orlmy-msg").blur(function(){
				var data = {"appid":$.appid,"target":$.target,"source":$.source,"msg":"","id":0}
				socket.emit('status', data);
			})
		}
		this.onMessageSeen = function(socket){
			//code for message seen
			//
		}
		this.onMessageDelivered = function(socket){
			//code for message delivered
		}

		this.onConnected = function(socket){
			var uniqueid = $.uniqueid();
			socket.emit('uniqueid', uniqueid);
		}

		this.row_chat_a = function(data){
      			
                var html = '<div class="card-row">'+
                  				'<div class="orlmy-card right">'+
                      				data.msg+
				                '</div>'+
				            '</div>';
                $(".chat-box-body").append(html);

            $.scrollStream();
    	}

    	this.row_chat_b = function(data){
      			var html = '<div class="card-row">'+
                  				'<div class="orlmy-card left">'+
                      				data.msg+
				                '</div>'+
				            '</div>';
                $(".chat-box-body").append(html);

                $.scrollStream();
                $.notify();//notify
    	}

	}

	function Xturs(){
			//controls the app structures
		this.init = function(){	

			this.uiControl();
			this.setOrlmyRow();
			this.onWindowResize();
			this.switchActiveUser();

		}
		this.setOrlmyRow = function()
		{
			var h = this.height;
			$(".app-fluid").css({height:h+"px"});
            $(".orlmy-row-1,.orlmy-row-2,.orlmy-row-3").css({height:h+"px"});
			//$(".orlmy-row-index").css({height:h+"px"});
			//console.log(h)
		}
		this.width = null;
		this.height = null;
		this.uiControl = function()
		{
			this.width = this.getWidth($(window));
			this.height = this.getHeight($(window));
			//the rest of layout control
			
			//ui for chat stream
			this.setMobileStream();
			//controll all height:
		}
		this.switchActiveUser = function(){//control who is chating

			var $this = this, chat_body = $(".chat-box-body"), chat_head = $(".chat-box-head");
			$("body").on("click", ".row-page", function(){
				var parent = $(this).parents(".contact-chat-list-row");
				var target_id = parent.attr("id");	
				var name = parent.find("h4").html();
				//try to check if user id equals active user id
				
				chat_body.html("");//clear chat stream
				//request chat history				
				chat_head.find("h4").attr("id", target_id);//set chat active id				
				chat_head.find("h4").html(name)//set user name

				//set active user as global
				$.target = target_id;
				console.log(target_id)

			});
		}
		this.setUserForActiveChat = function(e){
			// var uuid = e.parents("li").attr("id"), name = e.find("h5").text(), cnt = $(".active-user-conversation");
			// cnt.find("h4").html(name); cnt.attr("id", uuid);
			//get chat history				
			//socket.emit('chat_history', uuid);
		}
		this.headerHeight = 80;
		this.inputHeight = 90;
		this.setMobileStream = function(){
			var cStream = this.height - (this.headerHeight+this.inputHeight);
			$(".main-stream-wrap").css({height:cStream+"px"});
			$(".orlmy-row-2").removeClass("chat-stream").css({height:this.height+"px"});

			//adjust all row 
			this.setOrlmyRow();

		}
		this.onWindowResize = function(){
			var $this = this;
			$(window).resize(function(){
				$this.uiControl();
			});
		}
		this.getWidth = function(el){
			return el.width()
		}
		this.getHeight = function(el){
			return el.height();
		}
	}

	function Auths(){
		//resources such as files, images etc
		this.init = function(s)
		{
			this.auths(s);
			//this.signup(s);
			this.onServerAuthsReceived(s);
		}
		this.auths = function(socket){
			var $this = this;			
			var data = {"appid":$.appid, "host":"board.phynixmedia.com"}
			socket.emit('auths', data);			
		}
		this.onServerAuthsReceived = function(socket){
			var $this = this;
			socket.on("auths", function(msg){
				if(msg.login == 1){
					//$.set_userdata($.key, msg);
				}else{
					//show failed message
				} 
			})
		}
		this.signup = function(socket){
			$("#signup").click(function(){

			});
		}
		
		this.fgetv = function(formid, key, name)
		{
			var d = {}, f = $(formid).find(key);
			f.each(function()
			{
				var k = $(this).attr("name"), v = $(this).val();
				
				if($(this).val()!="" && !$(this).hasClass("ignore"))
				{
						d[k] = v;
				}
			});
			return d;

		}
	}

})(jQuery)

jQuery.extend({
	uniqueid:function(){
	  	return $.uucode;
	},
	appid:"$!jkhhgft7556ftyjdf@ghjghg",
	source:"OIT$!DES7",
	target:"TR43W%&YH",
	key:"dashhubdash",
	uucode:null,
	userdata:function(key)
	{
		var session = $.cookie(key);
		if(typeof(session) != 'undefined'){
			var data =  $.parseJSON(session);
			if(!data){ return ""; }else{ return data; }	
		}				
	},
	set_userdata:function(key, data)
	{
		$.cookie(key, JSON.stringify(data));
	},
	unset_userdata:function(key)
	{
		$.cookie(key, null);
	},
	randomid:function(){
		return Math.random().toString(36).split('').filter( function(value, index, self) { 
        return self.indexOf(value) === index;
    	}).join('').substr(2,8);
	},
	scrollStream:function(){

		var chatbody = $(".chat-box-body");
		chatbody.stop().animate({
		  	scrollTop: chatbody[0].scrollHeight
		}, 400);
	},
	notify:function()
	{
		//C:\xampp\htdocs\phynix_app\assets\vendors\orlmy\audio
    	new Audio('https://app.phynixmedia.co.uk/chat_app/mini-board/orlmy/audio/all-eyes-on-me.mp3').play()
	}
})

 $(function()
 { 
 	var host = "https://phynixmedia.com";
    $("body").orlmy({socket:io(host)});
});