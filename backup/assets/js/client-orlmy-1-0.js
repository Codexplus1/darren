(function($)
{
	$.fn.orlmy = function(options)
	{
		var defaults = {socket: null}
		var options =  $.extend(defaults, options);	

		var cls = null;
	    this.each(function(e)
	    { 	    	
	      //initialize the class
	      cls = new j(options.socket);
	    });
	    return cls;
	}

	//set up the class
var j = (function()
    {
      function j(s)
      {
          	var a = new App();
          	
          	//initialize Class
          	var Auth = a.initAuths();
			var Com = a.initCom();
			a.initXtures();

          	Auth.init(s, Com); //s equals socket
          	Com.init(s, Auth); //s equals socket
          	
      }

      
    //return class object
    return j;

  }());

	//controls the layout
	function App()
	{
		//app control
		this.initXtures = function()
		{
			new Xturs().init();
		}
		this.initCom = function(socket){
			return new Com();
			//this.uiData(socket, com);
		}
		this.initAuths = function(socket){
			return new Auths();
		}
		this.uiData = function(s, com)
		{
			var cookie_string = $.userdata($.key);
			if(typeof(cookie_string)!='undefined'){
				var c = cookie_string.res, user = $(".current-user");
				//set user name
				user.find("h4").html(c.name);
				//store user id in cookie
				$.uucode = c.uucode;
				//set the user id 
				user.attr("id",c.uucode);
			}			
			//this send unique static id associated 
			//to this user to the server
			com.onConnected(s);
			com.onDMMessageReceived(s);
		}

	}

	function Com(){
		//socket io control
		this.server = null;
		this.init = function(socket, auth){
			
			//chat btn control method
			this.onChatInit();
			this.onMessegeSend(socket);			
		}

		this.comEvents = function(socket)
		{
			var s = socket;
					
			this.onContactsReceived(s);			
			//blur and focus
			this.onTypeFocus(s);
			this.onTpypeBlur(s);
			this.onMessageReceived(s);			
			//status watch
			this.onStatusChanged(s);
			this.onConversationReceived(s);
			//call comtrol method
			this.onCall(s);
		}

		this.onCall = function(socket)
		{
			$(".fa-bell-o").click(function()
			{
				var data = {from:'+447480487479', to:'+447340960948'}
				socket.emit('call', data);
			});
		}

		this.initRequest = true;
		this.onChatInit = function()
		{
			var $this = this;
			var chat = $(".chat-btn"), istyping = $(".istyping")
	        chat.css({cursor:"pointer"})
	        chat.click(function()
	        {
	          	if(!$(this).hasClass("show")){
	          	    $(".chat-box").fadeIn();
	          	    $(this).addClass("show");
	          	    if($(this).hasClass("init"))
	          	    {
	          	    	var data = {"msg":"Hi! We are here to answer any question you have."};
	          	    	$this.initProcess(istyping, data);
	          	    	setTimeout(function()
	          	    	{
	          	    		var data = {"msg":"Please, Enter your name."};
	          	    		$this.initProcess(istyping, data);
	          	    	}, 1100);	          	    	
	          	    }
	          	    $(this).removeClass("init");
	          	    
	          	}else{
	          	    $(".chat-box").fadeOut();
	          	    $(this).removeClass("show");
	          	}
	        })
		}
		
		this.initProcess = function(istyping, data)
		{
			var $this = this; istyping.show();
			setTimeout(function()
			{
				$this.row_chat_b(data);
				istyping.hide();//done typing...
			}, 1000);
			
		}	

		this.onMessageReceived = function(socket)
		{
			console.log($.source)
			var $this = this;
			socket.on('dm_'+$.source, function(msg){
				$this.row_chat_b(msg);
			});
		}
		
		this.onConversationReceived = function(socket)
		{
			socket.on("chat_history", function(msg){
				console.log(msg);
			});
		}
		this.onDMMessageReceived = function(socket)
		{
			var $this = this;
			//var useruuid = $(".current-user").attr("id");
			
			var uniqueid = $.uniqueid();

			socket.on(uniqueid, function(msg){

					//check if message is for active user
					//else check the contact list
					//then send notification
					var useruuid = $(".active-user-conversation").attr("id");
					var senderid, targetid, message = null;
					senderid = msg.uucode_sender;
					targetid = msg.uucode_target;
					message  = msg.data;
					if(useruuid == senderid){						
						$this.row_chat_b(msg.data);
						$this.onMessageDelivered(socket);
					}
					//search contact list for matching ID
					var r_text = msg.data;
					$(".conversation-list").find("li#"+senderid).find(".status-msg-update").html(r_text);
				
			});
		}

		this.onContactsReceived = function(socket)
		{
			var $this = this;
			socket.on('contacts', function(msg){
				$this.logContacts(msg);
			});
		}

		this.logContacts = function(data){
			
			var html = "";

            for(e in data){
            	html  += '<li id="'+data[e].uucode+'" class="list-group-item d-flex justify-content-between align-items-center list-group-item-action">'+
                '<div class="media">'+
                    '<img class="d-flex mr-3 rounded-circle" src="assets/images/chat/user-3.png" alt="">'+
                        '<div class="media-body chat-media-body">'+
                            '<h5>'+data[e].fname+" "+data[e].mname+" "+data[e].lname+'</h5>'+
                        '<div class="text-muted smaller status-msg-update">Thank you for letting me..</div>'+
                    '</div>'+
                '</div>'+
                    '<span class="badge pull-right">'+
                        '3:40am<br />'+
                    	'<span class="badge badge-light">4</span>'+
                    '</span>'+
            '</li>';
            }

            $(".conversation-list").html(html);
		}

		this.onMessegeSend = function(socket){
			var $this = this;
			$("body").keypress(function(e)
			{
				if(e.which == 13)
				{
					$this.triggerRequest(socket);
					return false;
				}
			});

			$(".chat-box-btn-send").click(function()
			{
				$this.triggerRequest(socket);
			})
		}

		this.triggerRequest = function(socket)//get called with enter key and btn click
		{
			var msg = $('#orlmy-msg').val();
				if(msg != ""){	
					
					var data = {"appid":$.appid,"target":$.target,"msg":msg,"id":1}
					if(this.initRequest){	
						var cache = $.userdata($.key);					
						//check if user is thesame
						
						$.source = $.uuidv4();
						if(typeof(cache) != 'undefined'){ //check if cache value is not undefined
							if(cache.id == msg){//get user id
								$.source = cache.uuidv4; //set uuidv4
							}
						}
						//if user not stored use server uuidv4 received
						//set my user id for a reuse
						data.source = $.source;
						socket.emit('client_contact', data); this.initRequest = false;						
						//set data  for cache				
						var cache_msg = {"id":msg, "uuidv4":$.source}
						//set cache data
						$.set_userdata($.key, cache_msg);

						//initialize events : 
						this.comEvents(socket);	

					}else{
						//set uuidva as aditional field in the data for server
						data.source = $.source;
						socket.emit('dm_message', data);						
					}				
					this.row_chat_a(data);
					
				}
			$('#orlmy-msg').val('')

		}
		
		this.onStatusChanged = function(socket){
			var typing = $(".istyping");

			socket.on($.source, function(msg){
				switch(msg.id){
					case 0: typing.hide(); break;//end typing
					case 1: typing.show(); break;//start typing
					case 2:  break;//msg delivered
					case 3:  break;//msg seen/received
				}
				console.log($.source)
			});
		}

		this.onTypeFocus = function(socket){
			//on input field active
			$("#orlmy-msg").on("focus", function(){
				var data = {"appid":$.appid,"target":$.target,"source":$.source,"msg":"","id":1}
				socket.emit('status', data);
			});
		}
		this.onTpypeBlur =  function(socket){
			//code for on input field blur
			$("#orlmy-msg").blur(function(){
				var data = {"appid":$.appid,"target":$.target,"source":$.source,"msg":"","id":0}
				socket.emit('status', data);
			})
		}
		this.onMessageSeen = function(socket){
			//code for message seen
			//
		}
		this.onMessageDelivered = function(socket){
			//code for message delivered
		}

		this.onConnected = function(socket){
			var uniqueid = $.uniqueid();
			socket.emit('uniqueid', uniqueid);
		}

		this.row_chat_a = function(data){
      			
                var html = '<div class="card-row">'+
                  				'<div class="orlmy-card right">'+
                      				data.msg+
				                '</div>'+
				            '</div>';

                  $(".chat-box-body").append(html);

            $.scrollStream();
            //$.notify(); disabled for sender
    	}

    	this.row_chat_b = function(data){
      			var html = '<div class="card-row">'+
                  				'<div class="orlmy-card left">'+
                      				data.msg+
				                '</div>'+
				            '</div>';
                $(".chat-box-body").append(html);

                $.scrollStream();
                $.notify();
    	}

	}

	function Xturs(){
			//controls the app structures
		this.init = function(){	

			this.uiControl();
			this.setOrlmyRow();
			this.onWindowResize();
			this.switchChart();

		}
		this.setOrlmyRow = function()
		{
			var h = this.height;
			$(".app-fluid").css({height:h+"px"});
            $(".orlmy-row-1,.orlmy-row-2,.orlmy-row-3").css({height:h+"px"});
			//$(".orlmy-row-index").css({height:h+"px"});
			//console.log(h)
		}
		this.width = null;
		this.height = null;
		this.uiControl = function()
		{
			this.width = this.getWidth($(window));
			this.height = this.getHeight($(window));
			//the rest of layout control
			
			//ui for chat stream
			this.setMobileStream();
			//controll all height:
		}
		this.switchChart = function(){//control who is chating

			var $this = this;
			$("body").on("click", ".chat-media-body", function(){
				var id = $(this).parents("li").attr("id");				
				//ui flow
				var row1 = $(".orlmy-row-1"), row2 =  $(".orlmy-row-2"), row3 = $(".orlmy-row-3");
				if($this.width <= 480){//serve mobile
					row2.css({zIndex:1});					
				}//@endif

				//clear chat stream and request user messages
				$(".chat-stream-wrap").html("");
				//request chat history
				$this.setUserForActiveChat($(this));
			});
		}
		this.setUserForActiveChat = function(e){
			var uuid = e.parents("li").attr("id"), name = e.find("h5").text(), cnt = $(".active-user-conversation");
			cnt.find("h4").html(name); cnt.attr("id", uuid);
			//get chat history				
			socket.emit('chat_history', uuid);
		}
		this.headerHeight = 80;
		this.inputHeight = 90;
		this.setMobileStream = function(){
			var cStream = this.height - (this.headerHeight+this.inputHeight);
			$(".main-stream-wrap").css({height:cStream+"px"});
			$(".orlmy-row-2").removeClass("chat-stream").css({height:this.height+"px"});

			//adjust all row 
			this.setOrlmyRow();

		}
		this.onWindowResize = function(){
			var $this = this;
			$(window).resize(function(){
				$this.uiControl();
			});
		}
		this.getWidth = function(el){
			return el.width()
		}
		this.getHeight = function(el){
			return el.height();
		}
	}

	function Auths(){
		//resources such as files, images etc
		this.ComClass = null;
		this.init = function(s, com)
		{
			this.ComClass = com;
			this.auths(s);
			//this.signup(s);
			this.onServerAuthsReceived(s);
		}
		this.auths = function(socket){
			var $this = this;			
			var data = {"appid":$.appid, "host":"phynixmedia.com"}
			socket.emit('auths', data);			
		}
		
		this.onServerAuthsReceived = function(socket){
			var $this = this;
			socket.on("auths", function(msg){
				if(msg.login == 1){
					//keep the value in case this is a new user
					$this.ComClass.server = msg.uuidv4;				
				}else{
					//show failed message
				} 
			})
		}
		this.signup = function(socket){
			$("#signup").click(function(){

			});
		}
		
		this.fgetv = function(formid, key, name)
		{
			var d = {}, f = $(formid).find(key);
			f.each(function()
			{
				var k = $(this).attr("name"), v = $(this).val();
				
				if($(this).val()!="" && !$(this).hasClass("ignore"))
				{
						d[k] = v;
				}
			});
			return d;

		}
	}

})(jQuery)

jQuery.extend({
	uniqueid:function(){
	  	return $.uucode;
	},
	appid:"$!jkhhgft7875hgftyjdf@ghjghg",//constant from server
	source:null,//auto generated | store in cache
	target:"OIT$!DES7", //constant | receipient 
	key:"clienthubclient",
	uucode:null,
	userdata:function(key)
	{
		var session = $.cookie(key);
		if(typeof(session) != 'undefined'){
			var data =  $.parseJSON(session);
			if(!data){ return ""; }else{ return data; }	
		}				
	},
	uuidv4:function() {
	  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
	    var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
	    return v.toString(16);
	  });
	},
	set_userdata:function(key, data)
	{
		$.cookie(key, JSON.stringify(data));
	},
	unset_userdata:function(key)
	{
		$.cookie(key, null);
	},
	randomid:function(){
		return Math.random().toString(36).split('').filter( function(value, index, self) { 
        return self.indexOf(value) === index;
    	}).join('').substr(2,8);
	},
	scrollStream:function(){
		var chatbody = $(".chat-box-body");
		chatbody.stop().animate({
		  	scrollTop: chatbody[0].scrollHeight
		}, 800);
	},
	notify:function()
	{
    	new Audio('orlmy/audio/all-eyes-on-me.mp3').play()
	}
})

 $(function()
 { 
 	var host = "https://phynixmedia.com/";
    $("body").orlmy({socket:io(host)});
});