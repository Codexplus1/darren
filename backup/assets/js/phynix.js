
(function($)
{
	$.fn.phynix = function(options){ 

		var csp = null;
		this.each(function(e)
		{ 
			//initialize the class
			csp = new p();
		});
		return csp;
	}
	
	//set up the class
	var p = (function()
		{
			function p()//constructor
			{
				var $this = this;
				$this.sidebar_open($(window));
				$this.onresizepage();

				$(".btn").on("touchstart click", function()
				{
					if($(this).hasClass("ln")){
						$this.login();
					}
					
				})
			}
			p.prototype.width = "60px";
			p.prototype.sidebar_open = function(e)
			{
				var sidebar = $(".page-area > div:first-child");
				var content = $(".page-area > div:last-child");
				var wrap_sidebar = $(".wrap-sidebar");
				//if(e.width() >= 800 && e.width() <= 1000){
				// sidebar.addClass("sidebar-tablet-width")
				// content.addClass("content-tablet-width")
				// wrap_sidebar.addClass("fixed-width-250")
				// }else{
				// 	sidebar.removeClass("sidebar-tablet-width")
				// 	content.removeClass("content-tablet-width")
				// 	wrap_sidebar.removeClass("fixed-width-300")
				// }
			}
			p.prototype.sidebar_close = function()
			{

			}
			p.prototype.onresizepage = function()
			{
				var $this = this;
				$(window).resize(function()
				{
					$this.sidebar_open($(window));
				})
			}
			p.prototype.login = function()
			{
				var formid = "#login";
				var res = $.fget(formid, ".fn", "name");

				if(res.count == 0){
					var data = res.data;
					var server = $.getValues(data, $.path()+"auths/signup", e, "POST");
					server.done(function(json){
						alert(json)
						$.status(formid, "Authentication successful.", "success", true);
					});
					server.fail(function(){ 
						$.status(formid, "Authentication failed.", "warning", true);
					})
					//request auth
				}else{
					$.status(formid, "Can't Complete your request.", "danger", true);
				}

				return false;
			}



			//return class object
			return p;

		}());

})
(jQuery)



/* ------------------------------- Multi-Phase Ajax Request ------------------------------------- */

jQuery.extend({
		alertx:".alertx",
		path:function()
		{
			var url = $("body").attr("id");			
			return 	url;
		},
		isNumeric:function(value){ return /^\d+$/.test(value);},
		status:function(formid, message, flag, timeout)
		{
			var timer = timeout ? 3000 : 0; 
			var html = '<div class="alert alert-'+flag+'" data-role="alert">'+message+'</div>';
			$(formid).find(".report").html(html);
			setTimeout(function(){
				$(formid).find(".report").html("");
			}, timer);
		},
		fget:function(formid,key,name)
		{
			var d={},f=$(formid).find(key), counter = 0;
			f.each(function()
			{
				//collect all value
				//hold reference to required count
				var k = $(this).attr(name), v = $(this).val();
				
				if($(this).val()!=""){
					d[k] = v;
				}
				if($(this).hasClass("required") && $(this).val()=="")
				{
					counter++;
				}
			});
			return {data:d,count:counter};
		},
		getValues:function(data,url_,dom,method)
		{
			var result=null;
			return $.ajax({url:url_,data:data,type:method,async:true,beforeSend:$.onSend});
		},
		onSend:function(){

		},
		onComplete:function(){ },
		parseJson:function(data){var jsonString=$.parseJSON(data);return jsonString;},
		notice:function(dom, msg){dom.html(msg).fadeIn("fast");setTimeout(function(){	dom.html(msg).fadeOut("fast");}, 5000);},
		getWidth:function(dom){return dom.width();},
		getHeight:function(dom){ return dom.height(); } 
	});

/*---------------- INITIALIZE READY FUNCTION*/
$(function()
{
	$("body").phynix();
})