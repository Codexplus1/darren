<?php

function encrypt($string)
{
	$crypto =& get_instance();
	$crypto->load->library('encryption');
	return str_replace("/", "-", $crypto->encryption->encrypt($string));
}

function decrypt($string)
{
	$crypto =& get_instance();
	$crypto->load->library('encryption');
	return $crypto->encryption->decrypt(str_replace("-", "/", $string));
}

function checkEmail($input = false){

	if(!$input){ return false; }

	$regex = "^([0-9]{10,11})$";
	if (preg_match($regex, $input)) {
	    return true;
	} else {
	    return false;
	}
}

function setEncryptionKey(){

	$CI =& get_instance();
	if( session()->get('data-key') == NULL){

		$CI->config->config['encryption_key'] = bin2hex(openssl_random_pseudo_bytes(32));
		session()->put('data-key', $CI->config->config['encryption_key'] );
	}else{
		$CI->config->config['encryption_key'] = session()->get('data-key');
	}
}

function getEncryptionKey(){

	$CI =& get_instance();
	return $CI->config->config['encryption_key'];
}
?>