<?php

use Sessions\Session;

if ( ! function_exists('session'))
{
    function session(){
        
        return new Session();
    }
}