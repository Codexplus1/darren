<?php

	function dump($data_array = false){
		echo "<pre>";
	    var_dump($data_array);
	    echo "</pre>";
	}

	function trace($data_array = false){
		echo "<pre>";
	    print_r($data_array);
	    echo "</pre>";
	}

?>