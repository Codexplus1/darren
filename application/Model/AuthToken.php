<?php

namespace Model;

use Exceptions\DBExceptions;

class AuthToken extends \CI_Model {

    const HASHSALT = "6Y6R5TG";

    public function __construct()
    {
		$this->load->database();
    }
    
    public function set($hashed_token, $userid){

        $dateNow = date("Y-m-d H:i:s"); //2019-08-17 06:16:16
        $expires = date("Y-m-d H:i:s", strtotime("+15 minutes", strtotime($dateNow)));

        $parameter = [
            "userid" 	 	=> $userid,
            "token"			=> $hashed_token,
            "expires"		=> $expires,
        ];

        try{

            if($this->db->insert("auths_verify", $parameter)){
                return true;
            }
        
            throw new DBExceptions(DBExceptions::INVALID_ACCESS);

        }catch(DBExceptions $e){
            return $e->getMessage();
        }

        return false;
	}
    
    public function get($token = false){

        if(!$token) return false; 

        try{

            $dateNow = date("Y-m-d H:i:s");
            $query = $this->db->get_where('auths_verify', ['token'=>$token, "expires > "=>$dateNow]);
            
            if($query){
                
                if($user = $query->row_array()){

                    $user = (object) $user;
                    return $user->userid;
                }
            }
            
            throw new DBExceptions(DBExceptions::INVALID_ACCESS);

        }catch(DBExceptions $e){
            return $e->getMessage();
        }

        return false;

    }

}