<?php

namespace Model;

use Exceptions\DBExceptions;

class Auths extends \CI_Model {

        public function __construct()
        {
			$this->load->database();
		}

		public function createUser($newUserData, $hashed_password){

			$parameter = [
				"unique_code" 	 	=> substr(bin2hex(random_bytes(32)), 30),
				"namex"				=> htmlspecialchars($newUserData->get("fullname"), ENT_QUOTES, 'UTF-8'),
				"emailx"			=> htmlspecialchars($newUserData->get("email"), ENT_QUOTES, 'UTF-8'),
				"phonex"			=> htmlspecialchars($newUserData->get("phone"), ENT_QUOTES, 'UTF-8'),
				"passwordx" 		=> $hashed_password,
				"login_attempts" 	=> 0,
				"status" => 1
			];

			try{

				if($this->db->insert("auths", $parameter)){
					return true;
				}
			
				throw new DBExceptions(DBExceptions::INVALID_ACCESS);
	
			}catch(DBExceptions $e){
				return $e->getMessage();
			}

			return false;
		}
		
		/**
		 * @param $username
		 * Get user information using the username
		 */

		public function getUser($username = false){

			if(!$username) return false; 

			try{

				$query = $this->db->get_where('auths', ['emailx'=>$username]);
				
				if($query){

					return $query->row_array();
				}
				
				throw new DBExceptions(DBExceptions::INVALID_ACCESS);
	
			}catch(DBExceptions $e){
				
				return $e->getMessage();
			}

			return false;
		}

		/**
		 * @param $username 
		 */
		public function updateAttemts($username = false){

			if(!$username) return false;

			$this->db->where('emailx', $username);
			$this->db->set('login_attempts', 'login_attempts+1', FALSE);
			$this->db->update('auths');

		}
		
		/**
		 * @param $dataset, $target | update user information 
		 */

		public function update($dataset, $target){

			$this->db->where($target);
    		if($this->db->update("auths", $dataset)){
				return true;
			}
			return false;
		}

		public function delete($id){

		}

		public function coupleHash($hashed_password){

			$rand_hash = substr(bin2hex(openssl_random_pseudo_bytes(32)), 0, 10);
			return $hashed_password . $rand_hash;
		}

		public function decoupleHash($hashed_password){

			return substr($hashed_password, 0, -10);
		}

}

?>