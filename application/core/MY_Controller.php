<?php

if($_SERVER['HTTP_HOST'] === 'localhost'){
    error_reporting(E_ALL);
    ini_set('display_errors', 1);
    error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING );
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
}else{
    ini_set('display_errors', '0');
    error_reporting(E_ALL);
}

use Requests\Request;

class MY_Controller extends CI_Controller
{
    public $key = "c87sds78df09grfsdf7g67asyrgfhsdfgtysufhua7stgfyuad"; //have been relaced
    private $default_pass = NULL; // have been replaced
    public $unread_mail = "unread_mail";
    public $web_key = "YTGDRFWE890876YHTGEDFGH";
    public $web_contact = "876YHTGEDFGHYTGDRFWE890";
    public $menu_key = "890TGDRFW76YHTGEDFGHYE8";
    public $cart_key = 'my_cart_items';
    
    public function __construct(){
        parent::__construct();

        /** set random encryption key here */
        setEncryptionKey();

        ini_set( 'memory_limit', '200M' );
        ini_set('upload_max_filesize', '200M');  
        ini_set('post_max_size', '200M');  
        ini_set('max_input_time', 3600);  
        ini_set('max_execution_time', 3600);

        $this->load->helper('path_helper');

        $this->load->library('session');
        $this->load->model('templates_model');
        $this->load->model('company_model');
        $this->load->library('platform');
        $this->load->helper('url_helper');

    }

    public function getAppKey(){
        return $this->key;
    }

    public function request(){

        return (new Request)->request();
    }

    /**
     * Use this to generate and pass csrf token to the view
     */
    public function csrf(){

        return array(
            'name' => $this->security->get_csrf_token_name(),
            'hash' => $this->security->get_csrf_hash()
            );
    }

    public function getHeader()
    { 
        $data = self::getCompany();
    	$data["url"]  = base_url();
        $data['session_data'] = null;
        $data['activities'] = false;
        $data['plugins'] = [];
        $data['year'] = date("Y");
        $data["userdata"] = $this->session->userdata($this->key);
        $data['ADMIN_PATH'] = adminImagePath();
        $data['site_url'] = site_url("application/view/pages");
        $data['cart'] = 0;
        $data['status'] = null;

        if($data['userdata']){
            $data['namex'] = explode(" ", $data['userdata']['namex'])[0];
        }
    
    	return $data;
    }

    /*
    * get data that change page details */
    public function getCompany(){
        
        $userdata = $this->session->userdata($this->web_key);

        if(!$userdata){

            $company = $this->company_model->getCompany(array("status"=>1));

            if(count($company) > 0){
                
                $this->session->set_userdata($this->web_key, $company[0]);
            }
            $userdata = $this->session->userdata($this->web_key);
        }

        //$data = self::getContacts();

        $data['title'] = $userdata['title'];
        $data['description'] = $userdata['description'];
        $data['keywords'] = $userdata['keywords'];
        $data['template'] = "";
        $data['contact'] = $userdata['contact']??"";//check this

        return $data;
    }
}

?>