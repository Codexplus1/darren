<?php

namespace Sessions;

class Session  {

    protected $CI;

    function __construct() 
    {
        $this->CI =& get_instance();
    }

    /**
     * 
     */
    public function put($key = "ci_name", $data = NULL){
        
        $this->CI->session->set_userdata($key, $data);
    }

    /**
     * 
     */
    public function get($key = "ci_name"){

        return $this->CI->session->userdata($key)??NULL;
    }

    /**
     * 
     */
    public function forget($key = "ci_name"){

        $this->CI->session->unset_userdata($key);
    }

    /**
     * 
     */
    public function flash($key = "ci_name"){

        $flash = $this->get($key);
        $this->forget($key);
        return $flash;
    }

    /**
     * 
     */
    public function has($key = "ci_name"){

        return $this->CI->session->has_userdata($key);
    }

} 