<?php

class Cart_model extends CI_Model {

        private $admin_db = NULL;
		
        public function __construct()
        {
            $this->load->database();
            $this->admin_db = $this->load->database('admin', TRUE);
        }

        public function setCart ($parameters = false, $target = false)
		{
			$r = false;
			if($target){
				$this->admin_db->set($parameters);
				$this->admin_db->where($target);
				if($this->admin_db->update('shopping_cart')){
					$r = true;
				}
			}else{				
				if($this->admin_db->insert('shopping_cart', $parameters)){
					$r = $this->admin_db->insert_id();
				}	
			}			
			return $r;			
		}

		public function getCart ($parameters = false, $use_strict = true){

			$this->admin_db->select("cart.*, products.*");
			$this->admin_db->from('cart');
			$this->admin_db->join('products', 'products.product_id=cart.product_id', 'LEFT');


			if($parameters){
				$this->admin_db->where($parameters);
				$query = $this->admin_db->get();
				return $query->result_array();
			}else{
				if($use_strict){//set this to true so there will be no retuns if there is no match
					return false;
				}else{
					$query = $this->admin_db->get();
					return $query->result_array();
				}				
			}			
		}

		public function setOrders ($parameters = false, $target = false)
		{
			$r = false;
			if($target){
				$this->admin_db->set($parameters);
				$this->admin_db->where($target);
				if($this->admin_db->update('orders')){
					$r = true;
				}
			}else{				
				if($this->admin_db->insert('orders', $parameters)){
					$r = $this->admin_db->insert_id();
				}	
			}			
			return $r;			
		}
	
		public function setOrderDetails ($parameters = false)
		{
			$r = false;
			if($parameters){
				
				if($this->admin_db->insert_batch('order_details', $parameters)){
				
					$r = true;
				}
			}						
			return $r;			
		}
		
}

?>