<?php

class Mail_model extends CI_Model {

        public function __construct()
        {
                $this->load->database();
        }
		
		public function setMail($parameters = false, $target = false)
		{
			$r = false;
			if($target){
				$this->db->set($parameters);
				$this->db->where($target);
				if($this->db->update('mail_queue')){
					$r = true;
				}
			}else{				
				if($this->db->insert('mail_queue', $parameters)){
					$r = $this->db->insert_id();
				}	
			}			
			return $r;			
		}

		public function getMail($parameters = false, $use_strict = true){

			if($parameters){
				$query = $this->db->get_where('mail_queue', $parameters);
				return $query->result_array();
			}else{
				if($use_strict){//set this to true so there will be no retuns if there is no match
					return false;
				}else{
					$query = $this->db->get('mail_queue');
					return $query->result_array();
				}				
			}			
		}
}

?>