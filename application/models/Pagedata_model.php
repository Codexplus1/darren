<?php

class PageData_model extends CI_Model {

        public function __construct()
        {
                $this->load->database();
        }

        public function setBlocks ($parameters = false, $target = false)
		{
			$r = false;
			if($target){
				$this->db->set($parameters);
				$this->db->where($target);
				if($this->db->update('blocks')){
					$r = true;
				}
			}else{				
				if($this->db->insert('blocks', $parameters)){
					$r = $this->db->insert_id();
				}	
			}			
			return $r;			
		}

		public function getBlocks ($parameters = false, $use_strict = true){

			if($parameters){
				$query = $this->db->get_where('blocks', $parameters);
				return $query->result_array();
			}else{
				if($use_strict){//set this to true so there will be no retuns if there is no match
					return false;
				}else{
					$query = $this->db->get('blocks');
					return $query->result_array();
				}				
			}			
		}
		
		public function setGroups ($parameters = false, $target = false)
		{
			$r = false;
			if($target){
				$this->db->set($parameters);
				$this->db->where($target);
				if($this->db->update('groups')){
					$r = true;
				}
			}else{				
				if($this->db->insert('groups', $parameters)){
					$r = $this->db->insert_id();
				}	
			}			
			return $r;			
		}

		public function getGroups($parameters = false, $use_strict = true){

			if($parameters){
				$query = $this->db->get_where('groups', $parameters);
				return $query->result_array();
			}else{
				if($use_strict){//set this to true so there will be no retuns if there is no match
					return false;
				}else{
					$query = $this->db->get('groups');
					return $query->result_array();
				}				
			}			
		}

		public function setElements ($parameters = false, $target = false)
		{
			$r = false;
			if($target){
				$this->db->set($parameters);
				$this->db->where($target);
				if($this->db->update('elements')){
					$r = true;
				}
			}else{				
				if($this->db->insert('elements', $parameters)){
					$r = $this->db->insert_id();
				}	
			}			
			return $r;			
		}

		public function getElements($parameters = false, $use_strict = true){

			if($parameters){
				$query = $this->db->get_where('elements', $parameters);
				return $query->result_array();
			}else{
				if($use_strict){//set this to true so there will be no retuns if there is no match
					return false;
				}else{
					$query = $this->db->get('elements');
					return $query->result_array();
				}				
			}			
		}

		public function setNavPositions ($parameters = false, $target = false)
		{
			$r = false;
			if($target){
				$this->db->set($parameters);
				$this->db->where($target);
				if($this->db->update('nav_positions')){
					$r = true;
				}
			}else{				
				if($this->db->insert('nav_positions', $parameters)){
					$r = $this->db->insert_id();
				}	
			}			
			return $r;			
		}

		public function getNavPositions($parameters = false, $use_strict = true){

			if($parameters){
				$query = $this->db->get_where('nav_positions', $parameters);
				return $query->result_array();
			}else{
				if($use_strict){//set this to true so there will be no retuns if there is no match
					return false;
				}else{
					$query = $this->db->get('nav_positions');
					return $query->result_array();
				}				
			}			
		}

		
		
}

?>