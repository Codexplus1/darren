<?php

class Templates_model extends CI_Model {

	public function __construct()
    {
        $this->load->database();
    }

	public function setTemplates ($parameters = false, $target = false)
	{
		$r = false;
		if($target){
			$this->db->set($parameters);
			$this->db->where($target);
			if($this->db->update('templates')){
				$r = true;
			}
		}else{				
			if($this->db->insert('templates', $parameters)){
				$r = $this->db->insert_id();
			}	
		}			
		return $r;			
	}

	public function getTemplates ($parameters = false, $use_strict = true){

		if($parameters["url"] == "index"){ $parameters['url'] = "Home"; }
		if($parameters){
			$query = $this->db->get_where('templates', $parameters);
			return $query->result_array();
		}else{
			if($use_strict){//set this to true so there will be no retuns if there is no match
				return false;
			}else{
				$query = $this->db->get('templates');
				return $query->result_array();
			}				
		}			
	}

	public function getContacts($parameters = false, $use_strict = true ){
		if(!$parameters || (count($parameters) ==0)){ return false; }

		$this->db->select('*');
		$this->db->from('templates');
		$this->db->join('company_contacts', 'templates.unique_code=company_contacts.contact_unique_code', 'LEFT');
		
		$first = true;
		foreach ($parameters as $key => $value) {
			
			if($first){
				$this->db->like("url",$value);
				$first = false;
			}
			$this->db->or_like("url", $value);
		}
	
		$query = $this->db->get();
		return $query->result_array();
	}

}
