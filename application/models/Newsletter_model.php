<?php

class Newsletter_model extends CI_Model {

        public function __construct()
        {
                $this->load->database();
        }
		
		public function setNewsletter($parameters = false, $target = false)
		{
			$r = false;
			if($target){
				$this->db->set($parameters);
				$this->db->where($target);
				if($this->db->update('newsletter')){
					$r = true;
				}
			}else{				
				if($this->db->insert('newsletter', $parameters)){
					$r = $this->db->insert_id();
				}	
			}			
			return $r;			
		}

		public function getNewsletter($parameters = false, $use_strict = true){

			if($parameters){
				$query = $this->db->get_where('newsletter', $parameters);
				return $query->result_array();
			}else{
				if($use_strict){//set this to true so there will be no retuns if there is no match
					return false;
				}else{
					$query = $this->db->get('newsletter');
					return $query->result_array();
				}				
			}			
		}
}

?>