<?php

class Auths_model extends CI_Model {

        public function __construct()
        {
            $this->load->database();
		}
		
		/**
		 * Get user information using the username
		 */

		public function getUser($username = false){

			if(!$username){ return false; }

			$query = $this->db->get_where('auths', ['email'=>$username]);
			
			return $query->row_array();
		}
		
		/**
		 * @param $dataset, $target | update user information 
		 */

		public function update($dataset, $target){

		}

		public function delete($id){

		}
}

?>