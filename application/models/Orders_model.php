<?php

class Orders_model extends CI_Model {

        private $admin_db = NULL;
		
        public function __construct()
        {
            $this->load->database();
            $this->admin_db = $this->load->database('admin', TRUE);
        }

		public function setOrders ($parameters = false, $target = false)
		{
			$r = false;
			if($target){
				$this->admin_db->set($parameters);
				$this->admin_db->where($target);
				if($this->admin_db->update('orders')){
					$r = true;
				}
			}else{				
				if($this->admin_db->insert('orders', $parameters)){
					$r = $this->admin_db->insert_id();
				}	
			}			
			return $r;			
		}

		public function getOrders ($parameters = false, $use_strict = true){

			if($parameters){
				$query = $this->admin_db->get_where('orders', $parameters);
				return $query->result_array();
			}else{
				if($use_strict){//set this to true so there will be no retuns if there is no match
					return false;
				}else{
					$query = $this->admin_db->get('orders');
					return $query->result_array();
				}				
			}			
		}
	
		public function setOrderDetails ($parameters = false)
		{
			$r = false;
			if($parameters){
				
				if($this->admin_db->insert_batch('order_details', $parameters)){
				
					$r = true;
				}
			}						
			return $r;			
		}

		public function getOrderDetails ($parameters = false, $use_strict = true){

			if($parameters){
				$query = $this->admin_db->get_where('order_details', $parameters);
				return $query->result_array();
			}else{
				if($use_strict){//set this to true so there will be no retuns if there is no match
					return false;
				}else{
					$query = $this->admin_db->get('order_details');
					return $query->result_array();
				}				
			}			
		}

		public function getOrderDetailsAndInfo($parameters = false){

			if(!$parameters){ return false;}

			$this->admin_db->select('*');
			$this->admin_db->from('order_details');
			$this->admin_db->join('products', 'order_details.product_id=products.product_id', 'LEFT');
			$this->admin_db->join('product_sizes', 'order_details.size_id=product_sizes.size_id', 'LEFT');
			$this->admin_db->where($parameters);
			$query = $this->admin_db->get();
			return $query->result_array();
						
		}
		
}

?>