<?php

class Products_model extends CI_Model {

		private $admin_db = NULL;
		
        public function __construct()
        {
            $this->load->database();
            $this->admin_db = $this->load->database('admin', TRUE);
        }

       
		public function getProducts ($parameters = false, $use_strict = true){

			if($parameters){
				$query = $this->admin_db->get_where('products', $parameters);
				return $query->result_array();
			}else{
				if($use_strict){//set this to true so there will be no retuns if there is no match
					return false;
				}else{
					$query = $this->admin_db->get('products');
					return $query->result_array();
				}				
			}			
		}
	
		public function getProductPrice ($parameters = false, $all = false){

			$this->admin_db->order_by("selling_price", "asc");
			if($parameters){
				
				$query = $this->admin_db->get_where('product_prices', $parameters);
				if($all){
					return $query->result_array();
				}else{
					return $query->row_array();
				}
			}	
			return false;	
		}

		public function getProductQuantity ($parameters = false){

			if($parameters){
				$query = $this->admin_db->get_where('product_quantity', $parameters);
				return $query->row_array();
			}
			return false;			
		}

		public function getProductCategory ($parameters = false){

			if($parameters){
				$query = $this->admin_db->get_where('category', $parameters);
				return $query->result_array();
			}
			return false;			
		}

		public function getProductImages ($parameters = false){

			if($parameters){
				$query = $this->admin_db->get_where('product_images', $parameters);
				return $query->row_array();
			}
			return false;			
		}

		public function getAllProductImages ($parameters = false){

			if($parameters){
				$query = $this->admin_db->get_where('product_images', $parameters);
				return $query->result_array();
			}
			return false;			
		}

		public function getProductSizes($parameters = false, $full = false){

			if($parameters && $full){

				$this->admin_db->select('*');
				$this->admin_db->from('product_sizes');
				$this->admin_db->join('product_prices', 'product_prices.size_id=product_sizes.size_id', 'LEFT');
				$this->admin_db->where($parameters);
				$this->admin_db->order_by("size", "asc");
				$query = $this->admin_db->get();
				return $query->result_array();
			}else if($parameters){
				
				$query = $this->admin_db->get_where('product_sizes', $parameters);
				return $query->result_array();
			}
			return false;			
		}

		public function searchProducts ($parameters = false, $_key = "namex"){

			if($parameters)
			{
				$this->admin_db->select("*");
				$this->admin_db->from('products');
				
				$counter = 0;
				foreach ($parameters as $key => $keyword) {
					
					if($counter == 0){
						$this->admin_db->like($_key, $keyword);
					}else{
						$this->admin_db->or_like($_key, $keyword);
					}
					$counter++;
				}			

				$query = $this->admin_db->get();
				return $query->result_array();
			}
			return false;			
		}
		
}

?>