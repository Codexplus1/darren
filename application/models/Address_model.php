<?php

class Address_model extends CI_Model {

        private $admin_db = NULL;
		
        public function __construct()
        {
            $this->load->database();
            $this->admin_db = $this->load->database('admin', TRUE);
        }

		public function setAddress ($parameters = false, $target = false)
		{
			$r = false;
			if($target){
				$this->admin_db->set($parameters);
				$this->admin_db->where($target);
				if($this->admin_db->update('customers_contacts')){
					$r = true;
				}
			}else{				
				if($this->admin_db->insert('customers_contacts', $parameters)){
					$r = $this->admin_db->insert_id();
				}	
			}			
			return $r;			
		}

		public function getAddress ($parameters = false, $use_strict = true){

			if($parameters){
				$query = $this->admin_db->get_where('customers_contacts', $parameters);
				return $query->result_array();
			}else{
				if($use_strict){//set this to true so there will be no retuns if there is no match
					return false;
				}else{
					$query = $this->admin_db->get('customers_contacts');
					return $query->result_array();
				}				
			}			
		}
	
		
		
}

?>