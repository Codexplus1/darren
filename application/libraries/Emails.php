<?php
    


    class Emails{

    	public $session = NULL;
        public $email = NULL;

    	public function __construct()
    	{
    		ob_start();
		    include('index.php');
		    ob_end_clean();
		    $CI =& get_instance();
    	}

        public function getTemplate($file_path = false){
            if(!$file_path){ return false; }
            return file_get_contents($file_path);
        }

        public function setEnquiryConfirmation($params = false,  $data = false){
           
            $template_path = base_url().'html/templates/confirmation_to_enquiry.html';                        
            
            $template = self::getTemplate($template_path);
            $message = $template;

            $parameters = array(
                            "subject"=>$params['subject'],
                            "mailfrom"=>$params['from'],
                            "mailto"=>$params['to'],
                            "message"=>$message,
                            "mail_type"=>"Confirmation",
                            "status"=>1
                        );
            return $parameters;
            
        }

        public function setAdminConfirmation($params = false,  $data = false){
           
            $template_path = base_url().'html/templates/confirmation_to_admin.html';                        
            
            $template = self::getTemplate($template_path);
            $message = $template;

            //$message  = str_replace("__USER__", $data['user'], $template);

            $parameters = array(
                            "subject"=>$params['subject'],
                            "mailfrom"=>$params['from'],
                            "mailto"=>$params['to'],
                            "message"=>$message,
                            "mail_type"=>"Notification",
                            "status"=>1
                        );
            return $parameters;
            
        }

        public function setSponsorshipConfirmation($params = false,  $data = false){
            $template_path = base_url().'html/templates/confirmation_to_sponsorer.html';     
               
            $template = self::getTemplate($template_path);
            $message  = str_replace("__USER__", $data['target'], $template);
            $parameters = array(
                            "subject"=>$params['subject'],
                            "mailfrom"=>$params['from'],
                            "mailto"=>$params['to'],
                            "message"=>$message,
                            "mail_type"=>"Confirmation",
                            "status"=>1
                        );
            return $parameters;
        }

        public function setNominationConfirmation($params = false,  $data = false, $firstTemp = false){
            $template_path = base_url().'html/templates/confirmation_to_nominee.html';     
            if($firstTemp) {
                $template_path = base_url().'html/templates/confirmation_to_nominator.html';  
            }  
            $template = self::getTemplate($template_path);
            $message  = str_replace("__USER__", $data['target'], $template);
            $parameters = array(
                            "subject"=>$params['subject'],
                            "mailfrom"=>$params['from'],
                            "mailto"=>$params['to'],
                            "message"=>$message,
                            "mail_type"=>"Confirmation",
                            "status"=>1
                        );
            return $parameters;
        }

    }
?>