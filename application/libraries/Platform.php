<?php
    
    class Platform {

        public $CI;
        public $user_key = "78df09grfs";

    	public function __construct()
    	{

		    $this->CI =& get_instance();

            $this->CI->load->model('pagedata_model'); 
            $this->CI->load->model('products_model'); 
            $this->CI->load->model('templates_model');
            $this->CI->load->model('cart_model'); 

    	}
 
        public function getTemplate($url = false){

            $temp = $this->CI->templates_model->getTemplates(array("url"=>$url));
            if(!$temp){
                header("Location:".base_url()."page/notfound");
            }
            return $temp;
        }

        public function getPageLayout($layout_name = false){
            /*
            * check if file template .twig exits in database */
            $exists = file_exists(realpath(APPPATH . '/views/pages/' . $layout_name));
            
            if($exists){
                return true;
            }
            return false;
        }

        public function loadProductsCategory($product_id = false){
            /*
            **/
            $parameters = array("status"=>1, "parent"=>NULL);
            if($product_id){
                $parameters['product_id'] = $product_id;
            }

            $categories = $this->CI->products_model->getProductCategory($parameters);

            if($categories){

                $category_list = [];
                foreach ($categories as $key => $category) {
                    
                    $category['category'] = str_replace(" ", "-", str_replace(",", "", $category['category']));
                    $category['category_url'] = str_replace(" ", "-", str_replace(",", "", $category['url']));
                    $category_list[] = $category;
                }
                return $category_list;
            }
            return false;
        }

        public function getProductsByCategory($category_id = false, $isSingle = false){

            $categories = self::loadProductsCategory($category_id);

            //var_dump($categories);

            $categoriesList = array();

            if($categories){

                foreach ($categories as $key => $category) {

                    $category_id = $category['category_id'];

                    $parameters = array("category_id"=>$category_id, "status"=>1);
                    $products = $this->CI->products_model->getProducts($parameters);

                    if(count($products) !=0){

                        $product = $products[0];
                        $parameters = array("product_id"=>$product["product_id"]);
                        
                        $images = $this->CI->products_model->getProductImages($parameters);
                        if($images){

                            $product['images'] = $images["image_path"];
                            $product['category'] = strtoupper($category["namex"]);
                            $product['category_url'] = strtoupper($category["url"]);
                        }
                       $categoriesList[$category["namex"]] = $product;
                    }
                    
                }
            }

            return $categoriesList;
        }

        public function loadProducts($category_id = false){
        
        /*
        * */
        $parameters = array("public"=>1, "status"=>1);
        if($category_id){
            $parameters['category_id'] = $category_id;
        }

        $products = $this->CI->products_model->getProducts($parameters);

        if($products){

            $latest_products = array();
            $regular_products = array();

            foreach ($products as $key => $product) {
                
                $is_latest  = $product['is_latest'];
                $product_id = $product['product_id'];

                $parameters = array("product_id"=>$product_id);

                $product['quantity'] = $product['prices'] = false;

                /*
                * Get product quantity */
                $quantity = $this->CI->products_model->getProductQuantity($parameters);
                if($quantity){
                    $product['quantity'] = $quantity['quantity'];
                }

                /*
                * Get product selling price */
                $prices = $this->CI->products_model->getProductPrice($parameters, true);

                if($prices){

                    $price = $prices[0];
                    $product['price'] = $price['selling_price'];
                    $product['size_id'] = $price['size_id'];
                } 

                $product['sizes'] = $this->CI->products_model->getProductSizes($parameters);

                $product['images'] = $this->CI->products_model->getProductImages($parameters);
                $product['minmax_prices'] = self::setMinAndMaxPrice($prices);

                /*
                * set product to latest or regular */
                if($is_latest == 1){
                    $latest_products[] = $product;
                }
                
                $regular_products[] = $product;
                
            }
        }

            return array("latest"=>$latest_products, "regular"=>$regular_products);
        }


        public function setMinAndMaxPrice($prices = false){

            if(!$prices){ return ''; }

            if(count($prices) < 2){
                $minmax_prices = '';
            }else{
                $prices = array_column($prices, "selling_price");
                $minmax_prices = array("min"=>min($prices), "max"=>max($prices));
            }

            return $minmax_prices;
        }

        public function getPageData($page = false){
        if(!$page){ return false; }

        $unique_code = $page['unique_code'];

        //get template bocks
        $blocks = $this->CI->pagedata_model->getBlocks(array('template_code'=>$unique_code));
        
        //this holds all page data
        $pagedata = array();
        //use loop to get groups in each blocks
        foreach ($blocks as $key => $block) {

            $group_id = $block['block_id'];
            $groups = $this->CI->pagedata_model->getGroups(array('block_id'=>$group_id));

            /*
            * set flag to check for multiple block groups */
            $is_multipe = $block['multiple'];

            if($groups){

                $block['groups'] = array();
                
                foreach ($groups as $key => $group) {
                    
                    /*
                    * get elements */
                    $group_id = $group['group_id'];
                    $elements = $this->CI->pagedata_model->getElements(array("group_id"=>$group_id));

                    /*
                    * assign elements into element key*/
                    $group['elements'] = array();   
                    foreach ($elements as $key => $element) {
                        $group['elements'][$element['name']] = $element;
                    }

                    /*
                    * Get grpup Name */
                    $group_name = $group['group_name'];

                    /*
                    * check this for block groups */
                    if($is_multipe){
                        $block['groups'][] = $group;
                    }else{
                        $block['groups'][$group_name] = $group;
                    }
                    

                }
            }

            /*
            * reset blocks back to its holder array */
            $blockName = $block['block_name'];
            $pagedata[$blockName] = $block;
        }

        /*
        * assign blocks to it parent data array */
        $page['pagedata'] = $pagedata;

        return $page;
    }

    public function searchProducts($keywords){

        $products = $this->CI->products_model->searchProducts($keywords);
        

        if($products){

            $search_products = array();

            foreach ($products as $key => $product) {
                
                $is_latest  = $product['is_latest'];
                $product_id = $product['product_id'];

                $parameters = array("product_id"=>$product_id);

                $product['quantity'] = $product['prices'] = false;

                /*
                * Get product quantity */
                $quantity = $this->CI->products_model->getProductQuantity($parameters);
                if($quantity){
                    $product['quantity'] = $quantity['quantity'];
                }

                /*
                * Get product selling price */
                $price = $this->CI->products_model->getProductPrice($parameters);
                if($price){
                    $product['price'] = $price['selling_price'];
                } 

                $product['images'] = $this->CI->products_model->getProductImages($parameters);

                /*
                * set product to latest or regular */
                if($is_latest == 1){
                    $latest_products[] = $product;
                }
                
                $search_products[] = $product;
                
            }
        }

        return array("search_products"=>$search_products);

    }




}
?>