<?php

	 class Oauth{

    	public function __construct()
    	{
		
		}

		public function generateSalt($length = 5){
			return substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", $length)), 0, $length);
		}

		public function encrypt($password_raw = false, $salt = false){
			if(!$password_raw || !$salt) return false;
			return sha1(md5($password_raw).md5($salt));
		}
		public function decrypt(){

		}

		public function getToken($length){
		     $token = "";
		     $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		     $codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
		     $codeAlphabet.= "0123456789";
		     $max = strlen($codeAlphabet); // edited

		    for ($i=0; $i < $length; $i++) {
		        $token .= $codeAlphabet[random_int(0, $max-1)];
		    }

		    return $token;
		}

		public function unique_code(){
            $random_hash = bin2hex(random_bytes(32));
            return $random_hash;
        }

}

?>