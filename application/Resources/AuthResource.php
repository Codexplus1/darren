<?php

namespace Resources;

class AuthResource {

    /* --------------------------------------------------------------------------------------
            GENERAL AUTH MESSAGES
    -------------------------------------------------------------------------------------- */

    const ACCOUNT_BLOCKED           = "Account Blocked due to failed login attempts";
    const VERIFICATION_REQUIRED     = "Please, verify this is you!";
    const INVALID_LOGIN_ATTEMPT     = "Invalid login details";
    const USER_AREADY_EXISTS        = "User with similar records already exists!";
    const UNEXPECTED_ERROR          = "Sorry! Please, try again";
    const REGISTRATION_SUSSESS      = "Registration Successfully completed";
    const REGISTRATION_FAILED       = "Unable to complete registration";

    const VERIFICATION_SUCCESSFUL   = "Verification successful, Please, login with your details.";
    const VERIFICATION_FAILED       = "This verification has failed, Please, contact our support for help";
    const VERIFICATION_CODE_FAILED  = "Verification code is invalid";
    const SOME_INVALID_DETAILS      = "One or more fields has incorrrect data";
    

    /* --------------------------------------------------------------------------------------
            VERIFICATION EMAIL
    -------------------------------------------------------------------------------------- */

    const VERIFICATION_TITLE     = "Account Verification Code";
    const VERIFICATION_MESSAGE   = "You are receiving this as our measure to ensure your account is safe with us.
                                    Please, enter this code <strong>__CODE__</strong> to validate the login request is from you
                                    <br/> 
                                    Please Note that this code is only valid for <strong>15 minutes</strong>"; 
}