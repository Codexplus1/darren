<?php

error_reporting(0);
header('Access-Control-Allow-Origin: *');

class Error_404 extends CI_Controller {

	    public function __construct()
	    {
	          parent::__construct();
	          $this->load->helper('url_helper');
	    }

	    public function index()
	    {   
	        self::page404();
	    }

	    public function page404($slug =  false){

	        $data['title'] 		= "Samis Northampton, United Kingdom";
	        $data['page_title'] = "Page 404";
	        $data['url'] = base_url();
	        
	        $this->twig->display('pages/page_not_found.twig', array("data"=>$data)); 
	    }

	    public function Search($search =  false){
	        print "Search Related";
	    }
}

?>