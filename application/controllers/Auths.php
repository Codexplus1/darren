<?php


use Services\AuthService;
use Requests\CreateUserRequest;

class Auths extends My_Controller {

    protected $authService = NULL;

	public function __construct()
    {
        parent::__construct();

        $this->authService = new AuthService();
    }

    public function signup(){

        $data = parent::getHeader($this->session);

        $this->twig->display('auths/signup.twig', 
            [
                "data"=>$data, 
                "csrf"=>$this->csrf(), 
                "response"=>session()->flash("status")
            ]);
    }

    public function prosignup(){

        /** 
         * set this as a layer over services */
        (new CreateUserRequest())->validate();
    
        if($this->authService->create($this->request())){

            header("Location:". base_url() . "auths/login");
        }
        header("Location:". base_url() . "auths/signup");
    }

    public function login()
    {    
        $data = parent::getHeader();

        $this->twig->display('auths/login.twig', 
        [
            "data"=>$data, 
            "csrf"=>$this->csrf(),
            "response"=>session()->flash("status")
        ]);
    }

    public function verify(){
        
        
        $data = parent::getHeader();

        $this->twig->display('auths/verify.twig', 
        [
            "data"=>$data, 
            "csrf"=>$this->csrf(),
            "response"=>session()->flash("status")
        ]);
    }

    public function authverify(){

        if($this->authService->verify($this->request())){
            
            header("Location:". base_url() . "admin/dashboard");
            return;
        }
        header("Location:". base_url() . "auths/login");
    }

    public function prologin()
    {        
        
        if($this->authService->attempt($this->request())){

            //after all checks are done in Authenticate class and we want to further clarify this is a human not bot
            // we can redirect a user to the catcha page for further verifications - this is just to ckeck if user is human or bot
            
            header("Location:". base_url() . "admin/dashboard");
            return;
        }
        header("Location:". base_url() . "auths/login");
    }

    public function logout(){

        session()->forget(getEncryptionKey());

        AuthService::isAuthenticated();
    }

    
}

?>