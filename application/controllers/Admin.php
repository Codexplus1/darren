<?php

use Services\AuthService;

error_reporting(0);
header('Access-Control-Allow-Origin: *');

class Admin extends My_Controller {
    
    public $access = NULL;
       
    public function __construct()
    {
        parent::__construct();
        AuthService::isAuthenticated();
    }

    public function dashboard(){

        $data = parent::getHeader();

        $this->twig->display('pages/dashboard.twig', [
            "data"=>$data, 
            "user"=>session()->get(getEncryptionKey()),
            "authCheck" => AuthService::check()
        ]);
    }

}

?>