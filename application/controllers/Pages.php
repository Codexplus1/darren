<?php

use Services\AuthService;

error_reporting(0);
header('Access-Control-Allow-Origin: *');

class Pages extends MY_Controller {

	public function __construct()
    {
        parent::__construct();
        
        $this->load->library('session');      
        $this->load->helper('url_helper');	
    }

    public function view($url = 'index')
    {        
        AuthService::isAuthenticated();
    }
    
}

?>