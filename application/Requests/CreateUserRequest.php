<?php
namespace Requests;

use Requests\Request;
use Resources\AuthResource;

class CreateUserRequest extends Request{

    protected $rules = [

        'fullname' => 'required|min_length[3]|max_length[50]|trim',
        'phone' => 'required|min_length[3]|max_length[15]|numeric|trim',
        'email' => 'required|min_length[8]|max_length[100]|trim',
        'password' => 'required|min_length[8]|trim',
        'password_repeat' => 'required|min_length[8]|trim|matches[password]'
    ];

    public function __construct(){
        
        parent::__construct();

        $this->CI->load->library('form_validation');
    }

    public function validate(){

        foreach( $this->rules as $key => $rule )
        {
            $this->CI->form_validation->set_rules( $key, $this->get($key), $rule );
        }

        $this->redirect($this->CI->form_validation->run( $this ), AuthResource::SOME_INVALID_DETAILS);
    }

}