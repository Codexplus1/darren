<?php
namespace Requests;

class Request {

    private $data = NULL;
    protected $CI = NULL;

    public function __construct(){

        $this->CI =& get_instance();    
        $this->CI->load->library('user_agent');

        $this->crush(); 
    }

    /**
     * crush post data to object | convert post array to STD object
     * @return void
     */
    public function crush(){

        $this->data = (object) $_POST ?? $_GET;
    }

    /**
     * 
     */
    public function request(){

        return $this;
    }

    /**
     * @param $key string value
     * @return NULL or string value if found
     *  This check and get value using the key 
     *  if it exists in the post object
     */
    public function get($key = false){

        if(!$key) return false;
 
        if(property_exists($this->data, $key)){ return $this->data->$key; }
        
        return NULL;
    }

    /**
     * @param $key string value
     * @return boolean value 
     * This this check if key exists in the post object 
     */
    public function has($key = false){

        if(!$key || !$this->data) return false;

        if(property_exists($this->data, $key)){ return true; }

        return false;
    }

    /**
     * 
     */
    public function file(){

    }

    /**
     * @param $ruleStatus = validate response boolean
     * @param $message = status string | source could be direct string 
     * or from a Resource for dynamic messages
     */
    protected function redirect($ruleStatus, $message){

        if(!$ruleStatus)
        { 
            session()->put("status", ["key"=>"danger", "message"=>$message]);

            redirect($this->CI->agent->referrer()); 
        }
    }

}