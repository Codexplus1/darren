<?php

$config['twig']['template_dir'] = VIEWPATH;
$config['twig']['template_ext'] = 'php';
$config['twig']['environment']['autoescape'] = TRUE;
$config['twig']['environment']['cache'] = FALSE;
$config['twig']['environment']['debug'] = FALSE;

$config['template_dir'] = APPPATH.'views';
$config['cache_dir'] = APPPATH.'cache/twig';