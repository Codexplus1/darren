<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;

final class ProductsTest extends TestCase
{
    public function testCanBeCreatedFromValidProductsDetails(): void
    {
        $this->assertInstanceOf(
            Products::class,
            Products::details(1)
        );
    }

    public function testCannotBeCreatedFromInvalidProductsDetails(): void
    {
        $this->expectException(InvalidArgumentException::class);

        Products::details('invalid');
    }

    public function testCanBeUsedAsString(): void
    {
        $this->assertEquals(1,
            Products::details(1)
        );
    }
}





