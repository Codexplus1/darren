<?php
namespace Exceptions;

class RedirectException extends \Exception {

    protected $url;
    const ACCESS_DENIED  = "Access denied";

    public function __construct($url) {

        parent::__construct(ACCESS_DENIED, 301);
        $this->url = (string)$url;
    }

    public function getURL() {
        return $this->url;
    }

    public static function ThrowableException($exceptionType  = "default")
    {

        try {

            switch($exceptionType)
            {
                case "login": $path = "auths/login"; break;
                case "verify": $path = "auths/verify"; break;
                default: $path = base_url(); break;
            }

            throw new RedirectException(base_url() . $path);

        } catch (RedirectException $e) {

            header('Location: ' . $e->getURL());
        }

        die;
    }
}
