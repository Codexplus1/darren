<?php
namespace Exceptions;

class DBExceptions extends \Exception {

    /** 
     * flag for live or local
     * @environment = false means script is running on local */
    protected $environment = false;

    const ACCESS_DENIED = 101;
    const INVALID_ACCESS = 102;
    const OPERATION_FAILED = 103;

    public function __construct($errorCode, $code = 0, Exception $previous = null) {
        
        parent::__construct($this->DBException($errorCode), $code, $previous);
    }

    public function DBException($error_code = "UNKNOWN_ERROR") {

            switch ($error_code) {
                case self::ACCESS_DENIED:
                    $message = "Access denied";
                    break;
                case self::INVALID_ACCESS:
                    $message = "One or more details are invalid";
                    break;
                case self::OPERATION_FAILED:
                    $message = "Operation failed to complete";
                    break;
                default:
                    $message = "Unknown db error";
                    break;
            }
    
            if(!$this->environment){
                return $message;
            }else{
                //expland this to write to log if the script is running in live server
            }
        return $message = NULL;
    }
}