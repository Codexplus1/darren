<?php
namespace Services;

//load models
use Model\Auths;
use Resources\AuthResource;
use Services\AuthVerification;
use Exceptions\RedirectException;

class Authenticate {
    
    protected $auth = NULL;
    private $verify_auth_ok = 2;
    private $denial_of_access = 3;

    public function __construct(){

        /** Initialize auths model  */
        
        $this->auths = new Auths();
    }

    /**
     * Register new user account
     */
    public function register($request){

        if($this->auths->getUser($request->get("email"))){

            session()->put("status", ["key"=>"danger", "message"=>AuthResource::USER_AREADY_EXISTS]);
            
            return false;
        }

        return $this->auths->createUser($request, $this->makeHashPassword($request->get("password")));
    }

    /**
     * @param $password raw : Make pasword hash and coupled with random code
     * @return hashed password with extra digits
     */
    public function makeHashPassword($password){

        return $this->auths->coupleHash(password_hash($password, PASSWORD_BCRYPT, array('cost'=>12)));
    }

    /**
     * @param string $verifyCode
     * @return boolean
     */
    public function confirm($verifyCode){

        return (new AuthVerification)->checkVerification(trim($verifyCode));
    }

    /**
     * Use model to check if user exists
     * @return object $user
     */
    public function check($username = false){

        return $this->auths->getUser($username); 
    }

     /**
     * @param $request
     * This count the number of attempted failed login
     * here is where to compare the password and create session for the user if valid
     */
    public function attempt($request, $user){

        //get user stored one-way password
        $user = (object) $user;

        //check number of failed attempts
        if($user->login_attempts >= $this->denial_of_access){

            session()->put("status", ["key"=>"danger", "message"=>AuthResource::ACCOUNT_BLOCKED]);
            return false;
        }

        //get password from request
        $password = $request->get('password');

        if(password_verify($password, $this->auths->decoupleHash($user->passwordx)))
        {

            if($user->login_attempts >= $this->verify_auth_ok)
            {

                //set response massage 
                session()->put("status", ["key"=>"warning", "message"=>AuthResource::VERIFICATION_REQUIRED]);

                //send verification email
                (new AuthVerification)->setVerificationToken( $user )->sendToken();

                //redirect to verification page
                RedirectException::ThrowableException("verify");

                return false;
                
            }else if($user->login_attempts == 0)
            {

                $this->setUserData($user);
                return true;
            }
        }

        /** * Log failed attempts and lock account on several attempts */

        $this->failedAttempts($this->sanitize($request->get("username")));

        session()->put("status", ["key"=>"danger", "message"=>AuthResource::INVALID_LOGIN_ATTEMPT]);

        return false;
    }

    /**
     * @param string $username 
     * @return string $username, sanitized data
     */
    public function sanitize($username){

        return filter_var(strip_tags($username), FILTER_SANITIZE_STRING);
    }

    /**
     * Remove password from user data
     * @param $user - containing user data
    */
    private function setUserData($user){

        unset($user->passwordx);

        session()->put(getEncryptionKey(), $user);
    }

    /**
     * @param $value 
     * This lock a user if found after attempted failed login of up to 4 times
     */
    public function failedAttempts($username){

        $this->auths->updateAttemts($username);

        return true;
    }

    public static function authenticated(){
        
        if(session()->get(getEncryptionKey())){
            
            return true;
        }
        return false;
    }

    /**
     * @param $value 
     * this should kill the source completely if marked as vulnerable source
     */
    protected function killSession(){

        //this should black list the request source and block it from further request
    }

}