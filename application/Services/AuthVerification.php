<?php

namespace Services;

use Model\Auths;
use Model\AuthToken;
use Services\CURLRequest;
use Resources\AuthResource;

class AuthVerification {

    private $token = NULL;
    private $authtoken = NULL;
    private $isTokenCreated = false;
    private $userEmail = NULL;

    public function __construct(){

        /** Initialize authtoken model  */

        $this->authtoken = new AuthToken();
    }

    public function setVerificationToken( $user ){

        /** generate token */
        $this->token = strtoupper(substr(bin2hex(openssl_random_pseudo_bytes(5)), 5));

        /** log encrypted token to database */
        if($this->authtoken->set(crypt($this->token, AuthToken::HASHSALT), $user->auth_id)){

            $this->userEmail = $user->emailx;
            $this->isTokenCreated = true;
        }
        
        return $this;
    }

    /** Send token to user registered email via SES API using CURL */
    public function sendToken(){

        // send message to user registered email
        if($this->isTokenCreated){

            $curlRequest = new CURLRequest();
            
            $curlRequest->curl($curlRequest->setParams($this->userEmail, AuthResource::VERIFICATION_TITLE, 
                                                        str_replace("__CODE__", $this->token, AuthResource::VERIFICATION_MESSAGE)));
        }
    }

    /**
     * This validate user verification code
     */
    public function checkVerification($token){

        $encrypt_token = crypt($token, AuthToken::HASHSALT);

        if($userid = $this->authtoken->get($encrypt_token)){

            session()->put("status", ["key"=>"success", "message"=>AuthResource::VERIFICATION_SUCCESSFUL]);
            return (new Auths)->update(["login_attempts"=>0], ["auth_id"=>$userid]);

        }
        session()->put("status", ["key"=>"success", "message"=>AuthResource::VERIFICATION_FAILED]);
        return false;
    }


}