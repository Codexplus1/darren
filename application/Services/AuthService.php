<?php

namespace Services;

use Services\AuthValidator;
use Services\Authenticate;
use Resources\AuthResource;
use Exceptions\RedirectException;

class AuthService {

    private $authenticate = NULL;

    public function __construct(){

        $this->authenticate = new Authenticate();
    }

    public function check(){

        if( Authenticate::authenticated() ){ return true; }
        
        return false;
    }

    public static function isAuthenticated(){

        if( !Authenticate::authenticated() ){

            RedirectException::ThrowableException("login");
        }
    }

    public function create($request){
        
        try{
            /** 
            * password value */
            if(!AuthValidator::checkPassword($request->get("password"))){

                return false;
            }

            session()->put("status", ["key"=>"danger", "message"=>AuthResource::REGISTRATION_FAILED]);

            if($this->authenticate->register($request)){

                session()->put("status", ["key"=>"success", "message"=>AuthResource::REGISTRATION_SUSSESS]);
                return true;
            }

        }catch(Exception $e){
            //
        }
        return false;
    }

    public function verify($request){

        $authCode = htmlspecialchars($request->get("verify_code"), ENT_QUOTES, 'UTF-8');

        if($this->authenticate->confirm($authCode)){

            return true;
        }
        return false;
    }

    /**
     * @param $request
     * This trigger login request via helper class
     */
    public function attempt($request){

        /** Surrounded with try catch to hide code break information if anything happens */
        
        try{

            /** * Validate email and password value */

            if(!AuthValidator::validate($request)){

                return false;
            }

            /** * search database to see if user exists */

            if($user = $this->authenticate->check($this->authenticate->sanitize($request->get("username"))))
            {
                
                /** * Attempt to log user in if password is valid */
                if($this->authenticate->attempt($request, $user )){ return true; }

            }else{

                session()->put("status", ["key"=>"danger", "message"=>AuthResource::INVALID_LOGIN_ATTEMPT]);
            }
  
        }catch(Exception $e){

            /** * if something unexpected happen */
            session()->put("status", ["key"=>"danger", "message"=>AuthResource::UNEXPECTED_ERROR]);
        }

        return false;
    }
}