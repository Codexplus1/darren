<?php

namespace Services;

class CURLRequest{

    private $API_PATH = "https://9pxpgs4ad2.execute-api.us-east-2.amazonaws.com/prodpost";
    const MAIL_SENDER = "david.adebayo@cashbackapp.com";

    public function __construct(){

    }

    public static function setParams($userEmail, $subject, $message){

        return [
            "to"        => $userEmail,
            "from"      => CURLRequest::MAIL_SENDER,
            "subject"   => $subject,
            "message"   => $message
        ];

    } 

    public function curl($fields){

        $ch = curl_init();

        //set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $this->API_PATH);
        curl_setopt($ch, CURLOPT_POST, count($fields));
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields, true));
        //curl_setopt($ch, RETURNTRANSFER, 1);

        curl_exec($ch);

        $status = false;

        if(curl_errno($ch)){
            
            $status = (curl_getinfo($ch, CURLINFO_HTTP_CODE) == 200) ? true : false;
        }

        curl_close($ch);

        return $status;
    }
}