<?php
namespace Services;

class AuthValidator  {

    /**
     * @param $request
     * This validate the request values with expected type matched pattern
     */
    public static function validate($request){

        $username = filter_var($request->get("username"), FILTER_SANITIZE_STRING, FILTER_VALIDATE_EMAIL);
        $password = filter_var($request->get("password"), FILTER_SANITIZE_STRING);

        if(self::checkEmail($username) && self::checkPassword($password)){

            return $username;
        }
        return false;
    }

    /** Perform email check here */
    protected static function checkEmail($email){

        if(empty($email)){ return false; }

        $regex = "/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,10})$/";

        return preg_match ($regex, strtolower($email));
    }

    public static function checkPassword($password){

        $uppercase = preg_match('@[A-Z]@', $password);
        $lowercase = preg_match('@[a-z]@', $password);
        $number    = preg_match('@[0-9]@', $password);
        $specialChars = preg_match('@[^\w]@', $password);

        if(!$uppercase || !$lowercase || !$number || !$specialChars || strlen($password) < 8) {
            return false;
        }else{
            return true;
        }
    }


}