<?php 

interface AuthRepositoryInterface {

    public function attempt($request);
    
}