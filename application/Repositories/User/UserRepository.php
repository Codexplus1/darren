<?php
namespace Application\Repositories\Auth;

class AuthRepository implements AuthRepositoryInterface {

    public function __construct(){

        $this->load->library('session');
        $this->load->library('oauth');
        $this->load->model('auths_model');
    }

    /**
     * Use model to check if user exists
     */
    public function check($username){

        return $this->auths_model->getUser($username);
    }

    /** update login attempt */
}