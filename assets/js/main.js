/* =================================
------------------------------------
	Divisima | eCommerce Template
	Version: 1.0
 ------------------------------------
 ====================================*/


'use strict';


$(window).on('load', function() {
	/*------------------
		Preloder
	--------------------*/
	$(".loader").fadeOut();
	$("#preloder").delay(400).fadeOut("slow");

});

(function($) {
	/*------------------
		Navigation
	--------------------*/
	$('.main-menu').slicknav({
		prependTo:'.main-navbar .container',
		closedSymbol: '<i class="flaticon-right-arrow"></i>',
		openedSymbol: '<i class="flaticon-down-arrow"></i>'
	});


	/*------------------
		ScrollBar
	--------------------*/
	$(".cart-table-warp, .product-thumbs").niceScroll({
		cursorborder:"",
		cursorcolor:"#afafaf",
		boxzoom:false
	});


	/*------------------
		Category menu
	--------------------*/
	$('.category-menu > li').hover( function(e) {
		$(this).addClass('active');
		e.preventDefault();
	});
	$('.category-menu').mouseleave( function(e) {
		$('.category-menu li').removeClass('active');
		e.preventDefault();
	});


	/*------------------
		Background Set
	--------------------*/
	$('.set-bg').each(function() {
		var bg = $(this).data('setbg');
		$(this).css('background-image', 'url(' + bg + ')');
	});



	/*------------------
		Hero Slider
	--------------------*/
	var hero_s = $(".hero-slider");
    hero_s.owlCarousel({
        loop: true,
        margin: 0,
        nav: true,
        items: 1,
        dots: true,
        animateOut: 'slideOutLeft',
    	animateIn: 'slideInRight',
        navText: ['<i class="flaticon-left-arrow-1"></i>', '<i class="flaticon-right-arrow-1"></i>'],
        smartSpeed: 1200,
        autoHeight: false,
        autoplay: true,
        onInitialized: function() {
        	var a = this.items().length;
            $("#snh-1").html("<span>1</span><span>" + a + "</span>");
        }
    }).on("changed.owl.carousel", function(a) {
        var b = --a.item.index, a = a.item.count;
    	$("#snh-1").html("<span> "+ (1 > b ? b + a : b > a ? b - a : b) + "</span><span>" + a + "</span>");

    });

	hero_s.append('<div class="slider-nav-warp"><div class="slider-nav"></div></div>');
	$(".hero-slider .owl-nav, .hero-slider .owl-dots").appendTo('.slider-nav');



	/*------------------
		Brands Slider
	--------------------*/
	$('.product-slider').owlCarousel({
		loop: true, //$('.owl-carousel .item').length > 1 ? true:false,
		nav: true,
		dots: false,
		margin : 30,
		autoplay: true,
		navText: ['<i class="flaticon-left-arrow-1"></i>', '<i class="flaticon-right-arrow-1"></i>'],
		responsive : {
			0 : {
				items: 1,
			},
			480 : {
				items: 2,
			},
			768 : {
				items: 3,
			},
			1200 : {
				items: 4,
			}
		}
	});

	/*------------------
		Brands Slider
	--------------------*/
	$('.recently-viewed-slider').owlCarousel({
		loop: true, //$('.owl-carousel .item').length > 1 ? true:false,
		nav: true,
		dots: false,
		margin : 30,
		autoplay: true,
		navText: ['<i class="flaticon-left-arrow-1"></i>', '<i class="flaticon-right-arrow-1"></i>'],
		responsive : {
			0 : {
				items: 1,
			},
			480 : {
				items: 1,
			},
			768 : {
				items: 1,
			},
			1200 : {
				items: 1,
			}
		}
	});



	/*------------------
		Popular Services
	--------------------*/
	$('.popular-services-slider').owlCarousel({
		loop: true,
		dots: false,
		margin : 40,
		autoplay: true,
		nav:true,
		navText:['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
		responsive : {
			0 : {
				items: 1,
			},
			768 : {
				items: 2,
			},
			991: {
				items: 3
			}
		}
	});


	/*------------------
		Accordions
	--------------------*/
	$('.panel-link').on('click', function (e) {
		$('.panel-link').removeClass('active');
		var $this = $(this);
		if (!$this.hasClass('active')) {
			$this.addClass('active');
		}
		e.preventDefault();
	});


	/*-------------------
		Range Slider
	--------------------- */
	var rangeSlider = $(".price-range"),
		minamount = $("#minamount"),
		maxamount = $("#maxamount"),
		minPrice = rangeSlider.data('min'),
		maxPrice = rangeSlider.data('max');
	rangeSlider.slider({
		range: true,
		min: minPrice,
		max: maxPrice,
		values: [minPrice, maxPrice],
		slide: function (event, ui) {
			minamount.val('$' + ui.values[0]);
			maxamount.val('$' + ui.values[1]);
		}
	});
	minamount.val('$' + rangeSlider.slider("values", 0));
	maxamount.val('$' + rangeSlider.slider("values", 1));


	/*-------------------
		Quantity change
	--------------------- */
    var proQty = $('.pro-qty');
	proQty.prepend('<span class="dec qtybtn">-</span>');
	proQty.append('<span class="inc qtybtn">+</span>');
	proQty.on('click', '.qtybtn', function () {
		var $button = $(this);
		var oldValue = $button.parent().find('input').val();
		if ($button.hasClass('inc')) {
			var newVal = parseFloat(oldValue) + 1;
		} else {
			// Don't allow decrementing below zero
			if (oldValue > 1) {
				var newVal = parseFloat(oldValue) - 1;
			} else {
				newVal = 1;
			}
		}
		$button.parent().find('input').val(newVal);
	});



	/*------------------
		Single Product
	--------------------*/
	$('.product-thumbs-track > .pt').on('click', function(){
		$('.product-thumbs-track .pt').removeClass('active');
		$(this).addClass('active');
		var imgurl = $(this).data('imgbigurl');
		var bigImg = $('.product-big-img').attr('src');
		if(imgurl != bigImg) {
			$('.product-big-img').attr({src: imgurl});
			$('.zoomImg').attr({src: imgurl});
		}
	});


	$('.product-pic-zoom').zoom();

	/*------------------
		ADD TO CART
	--------------------*/

	var _path = $("body").attr("id");	

	$('.add_to_cart').click(function(){

		var $this = this; 

		var product_item = $(this).parents(".product-item-data").attr('id');
		
		var params = product_item.split("|");

		var price  = params[1], id = params[0], item_name = params[2], item_image = params[3], size_id = params[4], size = params[5];
		var data = {"product_id":id, "price":price, "quantity":"1", "namex":item_name, "image":item_image, "size_id":size_id,"size":size}


		processAddToCart(data, _path);
		
		return false;
	});

	$('.details_add_to_cart').click(function(){

		var $this = this; 

		var parent = $(this).parents(".product-item-data");
		var product_item = parent.attr('id');

		// var size_id = parent.find('.psizes:checked').attr('id');
		// var size_price = parent.find('.psizes:checked').val();
		var size_price = parent.find('.item-price').val();

		var size_price = size_price.split('|');
		var size_id = size_price[2];

		var params = product_item.split("|");

		var id = params[0], item_name = params[2], item_image = params[3];
		var price = size_price[1];
		var size = size_price[0];

		var quantity = $('.pro-qty').find('input').val();

		var data = {"product_id":id, "price":price, "quantity":quantity, "namex":item_name, 
					"image":item_image, "size_id":size_id,"size":size}

		processAddToCart(data, _path);
		
		return false;
	});

	function processAddToCart(data, url){

		var res = $.getValues(data, url+"shopping/pro_add_to_cart", null, "post");
		
		res.done(function(json)
		{
			try{
				if(json != null){
					var data = $.parseJSON(json);
					$('.shopping-card span').html(data.length);

					controlModal('Item successfully added to cart!');
				}
			}catch(e){

			}
		});
		res.fail(function(json)
		{

		});
	}

	$('.pro-qty').click(function(){

		var product_id = $(this).attr('id');

		var currency = '£';
		var quantity = $(this).find('input').val();
		var ref_price = $(this).parents('tr').find('.price h4');
		var price = ref_price.attr("id");

		var ref_size = $(this).parents('tr').find('.size h4');
		var size_id = ref_size.attr("id");

		//update price with quantity
		var sum = price * quantity
		var toFixSumTo2 = sum.toFixed(2)
		ref_price.html(currency+toFixSumTo2)

		var data = {"product_id":product_id, "size_id":size_id, "price":toFixSumTo2, "quantity":quantity}
		var res = $.getValues(data, _path+"shopping/pro_update_cart", $(this), "post");
		res.done(function(json)
		{
			try{
				var res_ = $.parseJSON(json);
				if(res_.status == "success"){

					var total = res_.amount.toFixed(2)

					$('.total-cost h6 span').html(currency+total);
				}
			}catch(e){

			}
		});
		res.fail(function(json)
		{
			//alert(5678)
		});
	});


	$('.remove').click(function(){

		var parent = $(this).parents('tr');
		var product_id = $(this).attr('id');
		var size_id = parent.find('.size h4').attr('id');

		var data = {"product_id":product_id, "size_id":size_id}
		var res = $.getValues(data, _path+"shopping/pro_remove_cart_item", $(this), "post");
		res.done(function(json)
		{
			try{
				var res_ = $.parseJSON(json);
				if(res_.status == "success"){
					document.location.href = document.location.href
				}
			}catch(e){

			}
		});

	});

	$('.subscribe').click(function(){

		var newsletter = $('.newsletter').val();
		if(newsletter){

			var data = {'emailx':newsletter}
			var res = $.getValues(data, _path+"services/pro_subscribe_to_newsletter", $(this), "post");
			res.done(function(json)
			{
				try{
					var res_ = $.parseJSON(json);
					if(res_.key == "success"){
						controlModal(res_.message);
						$('.newsletter').val("");
					}else{
						controlModal('Can\'t subscribe your email to our newletter');
					}
				}catch(e){

				}
			});
			res.fail(function(){
				controlModal('Newsletter subscription error');
			})
		}		
	});

	$('.item-price').change(function(){

		var v = $(this).val();
		var data = v.split('|');
		$('.p-price').attr('id', data[1]).html('£'+data[1])
	});

	$('.address_to_use').click(function(){

		var names = $('[name="names"]');
		var address_lin_1 = $('[name="address_lin_1"]');
		var address_line_2 = $('[name="address_line_2"]');
		var country = $('[name="country"]');
		var postcode = $('[name="postcode"]');
		var email_address =	$('[name="email_address"]');
		var phone_number = $('[name="phone_number"]');

		if( $(this).hasClass('use_stored_address')){
			try{
				var json_string = $('.stored_addresses').html();
				var json = $.parseJSON(json_string)
				if(json != null){

					var address = json[0];
					names.val(address.names)
					address_lin_1.val(address.address_1)
					address_line_2.val(address.address_2)
					country.val(address.country)
					postcode.val(address.postcode)
					email_address.val(address.email_address);
					phone_number.val(address.phone_number);

				}
			}catch(e){}
		}else{
			names.val("")
			address_lin_1.val("")
			address_line_2.val("")
			country.val("")
			postcode.val("")
			email_address.val("");
			phone_number.val("");
		}
	})

	// $('.item-price').click(function(){

	// 	var v = $(this).find('input').val();
	// 	var data = v.split('|');
	// 	$('.p-price').attr('id', data[1]).html('£'+data[1])
	// });

	function controlModal(message){
		/* 
		* Update the modal*/
		$('#notify').find(".modal-body p").html(message);
		$('#notify').modal()

		$('#notify').on('hidden.bs.modal', function (e) {
		  	$('#notify').find(".modal-body p").html("");
		})
	}

})(jQuery);

//Multi-Phase Ajax Request
jQuery.extend({alertx:".alertx",isNumeric:function(value){ return /^\d+$/.test(value);},getValues:function(data,url_,dom,method){var result=null;return $.ajax({url:url_,data:data,type:method,async:true,beforeSend:$.onSend});},onSend:function(){},onComplete:function(){ },parseJson:function(data){var jsonString=$.parseJSON(data);return jsonString;},notice:function(dom, msg){dom.html(msg).fadeIn("fast");setTimeout(function(){	dom.html(msg).fadeOut("fast");}, 5000);},getWidth:function(dom){return dom.width();},getHeight:function(dom){ return dom.height(); } });


